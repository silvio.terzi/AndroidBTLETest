package com.btletest.com.androidbtletest;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class DeviceActivity extends Activity
{
    private final static String TAG = DeviceActivity.class.getSimpleName();
    private final static int START_DELAY = 2000;
    private boolean startwait = false;

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    private TextView mConnectionState;
    private TextView mDataField;
    private TextView mDataRead;
    private ImageView lamp;
    private ImageView lock;
    private Button command;
    private Button unreliable;
    private Button connect;
    private Button scanservice;
    private Button beginwrite;
    private Button write;
    private Button execwrite;
    private Button read;
    private Button disconnect;
    private String mDeviceName;
    private String mDeviceAddress;
    private boolean mConnected = false;
    private boolean mSequence = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private BluetoothLeScanner mBluetoothLeScanner;
    private BluetoothLeService mBluetoothLeService;
    private ExpandableListView mGattServicesList;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothManager mBluetoothManager;

    private RadioGroup scangroup;
    private RadioButton newscan;
    private RadioButton oldscan;
    private boolean scantypenew = true;
    private boolean noreliable = false;

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";

    private int lockState = -1;

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
//            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
                mDataRead.setText(R.string.no_data);
                clearUI();

                if(mSequence)
                {
                    mBluetoothLeService.discoverGattServices();
                }
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
                command.setClickable(true);
                mSequence = false;
                noreliable = false;
                Log.d("TEST", "Command finished");
                clearUI();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                displayGattServices(mBluetoothLeService.getSupportedGattServices());

                if(mSequence)
                {
                    byte[] value = null;

                    switch (lockState)
                    {
                        case -1:
                            // nothing to do, state undefined
                            break;
                        case 0:
                            value = new byte[20];
                            value[0] = (byte) 0;
                            value[1] = (byte) 0;
                            value[2] = (byte) 22;
                            value[3] = (byte) 0;
                            value[4] = (byte) 2;

                            for (int ind = 5; ind < value.length; ind++)
                            {
                                value[ind] = (byte) 0;
                            }
                            break;
                        default:
                            value = new byte[20];
                            value[0] = (byte) 0;
                            value[1] = (byte) 0;
                            value[2] = (byte) 23;
                            value[3] = (byte) 0;
                            value[4] = (byte) 2;

                            for (int ind = 5; ind < value.length; ind++)
                            {
                                value[ind] = (byte) 0;
                            }
                            break;
                    }

                    BluetoothGattCharacteristic characteristic = mGattCharacteristics.get(3).get(0);

                    if (value != null)
                    {
                        if(! noreliable)
                            mBluetoothLeService.beginReliableWrite();
                        mBluetoothLeService.writeCharacteristic(characteristic, value);
                        Log.d("TEST", "Write sent");
                    }
                }
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action))
            {
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));

                if(mSequence)
                {
                    mBluetoothLeService.disconnect();
                    Log.d("TEST", "Read ok, Disconnect");
                }
            }
            else if (BluetoothLeService.ACTION_DATA_UNAVAILABLE.equals(action))
            {
                displayData(getResources().getString(R.string.no_data));

                if(mSequence)
                {
                    mBluetoothLeService.disconnect();
                    Log.d("TEST", "Read ko, Disconnect");
                }
            }
            else if (BluetoothLeService.OPERATION_EXECUTED.equals(action))
            {
                BluetoothGattCharacteristic characteristic = mGattCharacteristics.get(3).get(0);

                if(mSequence)
                {
//                mBluetoothLeService.disconnect();
//                Log.d("TEST", "Write ok, Disconnect");
//                mBluetoothLeService.readCharacteristic(characteristic);
                    if(! noreliable)
                    {
                        mBluetoothLeService.execReliableWrite();
                        Log.d("TEST", "Write ok, Reliable sent");
                    }
                    else
                    {
                        mBluetoothLeService.readCharacteristic(characteristic);
                        Log.d("TEST", "Write ok, Read sent");
                    }
                }
            }
            else if (BluetoothLeService.OPERATION_FAILED.equals(action))
            {
                displayData(getResources().getString(R.string.no_data));

                if(mSequence)
                {
                    mBluetoothLeService.disconnect();
                    Log.d("TEST", "Write failed, Disconnect");
                }
            }
            else if (BluetoothLeService.RELIABLE_EXECUTED.equals(action))
            {
                if(mSequence)
                {
                    BluetoothGattCharacteristic characteristic = mGattCharacteristics.get(3).get(0);
//                mBluetoothLeService.disconnect();
//                Log.d("TEST", "Write ok, Disconnect");
                    mBluetoothLeService.readCharacteristic(characteristic);
                    Log.d("TEST", "Write reliable ok, Read sent");
                }
            }
            else if (BluetoothLeService.RELIABLE_FAILED.equals(action))
            {
                displayData(getResources().getString(R.string.no_data));

                if(mSequence)
                {
                    mBluetoothLeService.disconnect();
                    Log.d("TEST", "Write reliable failed, Disconnect");
                }
            }
        }
    };

    // If a given GATT characteristic is selected, check for supported features.  This sample
    // demonstrates 'Read' and 'Notify' features.  See
    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
    // list of supported characteristic features.
    private final ExpandableListView.OnChildClickListener servicesListClickListner =
            new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                            int childPosition, long id) {
                    if (mGattCharacteristics != null) {
                        final BluetoothGattCharacteristic characteristic =
                                mGattCharacteristics.get(groupPosition).get(childPosition);
                        final int charaProp = characteristic.getProperties();
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
//                            if (mNotifyCharacteristic != null) {
//                                mBluetoothLeService.setCharacteristicNotification(
//                                        mNotifyCharacteristic, false);
//                                mNotifyCharacteristic = null;
//                            }
                            mBluetoothLeService.readCharacteristic(characteristic);
                        }
//                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
//                            mNotifyCharacteristic = characteristic;
//                            mBluetoothLeService.setCharacteristicNotification(
//                                    characteristic, true);
//                        }
                        return true;
                    }
                    return false;
                }
            };

    private void clearUI() {
        mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
        mDataField.setText(R.string.no_data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        // Sets up UI references.
        ((TextView) findViewById(R.id.device_address)).setText(mDeviceAddress);
        mGattServicesList = (ExpandableListView) findViewById(R.id.gatt_services_list);
        mGattServicesList.setOnChildClickListener(servicesListClickListner);
        mConnectionState = (TextView) findViewById(R.id.connection_state);
        mDataField = (TextView) findViewById(R.id.data_value);
        mDataRead = (TextView) findViewById(R.id.dataread);
        lamp = (ImageView) findViewById(R.id.lamp);
        lock = (ImageView) findViewById(R.id.lock);
        command = (Button) findViewById(R.id.sendcommand);
        connect = (Button) findViewById(R.id.connect);
        scanservice = (Button) findViewById(R.id.scanservice);
        beginwrite = (Button) findViewById(R.id.beginwrite);
        write = (Button) findViewById(R.id.write);
        execwrite = (Button) findViewById(R.id.execwrite);
        read = (Button) findViewById(R.id.read);
        disconnect = (Button) findViewById(R.id.disconnect);
        scangroup = (RadioGroup) findViewById(R.id.scangroup);
        newscan = (RadioButton) findViewById(R.id.newscan);
        oldscan = (RadioButton) findViewById(R.id.oldscan);

        newscan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                RadioButton me = (RadioButton) v;

                if(me.isChecked() && ! scantypenew)
                {
                    scantypenew = true;
                    if(startwait)
                        stopOldScan();
                    startBTScan();
                }
            }
        });

        oldscan.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                RadioButton me = (RadioButton) v;

                if(me.isChecked() && scantypenew)
                {
                    scantypenew = false;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                        {
                            if(mBluetoothLeScanner != null)
                            {
                                mBluetoothLeScanner.stopScan(mScanCallback);
                            }
                            startBTScan();
                        }
                }
            }
        });

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        {
            scangroup.setVisibility(View.GONE);
        }

        command.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
            Log.d("TEST", "Command started");
            mSequence = true;
            mBluetoothLeService.connect(mDeviceAddress);
            command.setClickable(false);
            }
        });

        unreliable.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.d("TEST", "Command started");
                mSequence = true;
                noreliable = true;
                mBluetoothLeService.connect(mDeviceAddress);
                command.setClickable(false);
            }
        });

        connect.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mBluetoothLeService.connect(mDeviceAddress);
                Log.d("TEST", "Connect");
            }
        });

        scanservice.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mBluetoothLeService.discoverGattServices();
                Log.d("TEST", "Discover services");
            }
        });

        beginwrite.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mBluetoothLeService.beginReliableWrite();
                Log.d("TEST", "Begin reliable write");
            }
        });

        write.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                byte[] value = null;

                switch (lockState)
                {
                    case -1:
                        // nothing to do, state undefined
                        break;
                    case 0:
                        value = new byte[20];
                        value[0] = (byte) 0;
                        value[1] = (byte) 0;
                        value[2] = (byte) 22;
                        value[3] = (byte) 0;
                        value[4] = (byte) 2;

                        for (int ind = 5; ind < value.length; ind++)
                        {
                            value[ind] = (byte) 0;
                        }
                        break;
                    default:
                        value = new byte[20];
                        value[0] = (byte) 0;
                        value[1] = (byte) 0;
                        value[2] = (byte) 23;
                        value[3] = (byte) 0;
                        value[4] = (byte) 2;

                        for (int ind = 5; ind < value.length; ind++)
                        {
                            value[ind] = (byte) 0;
                        }
                        break;
                }

                BluetoothGattCharacteristic characteristic = mGattCharacteristics.get(3).get(0);

                if (value != null)
                {
                    mBluetoothLeService.beginReliableWrite();
                    mBluetoothLeService.writeCharacteristic(characteristic, value);
                    Log.d("TEST", "Write");
                }
            }
        });

        execwrite.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mBluetoothLeService.execReliableWrite();
                Log.d("TEST", "Exec reliable write");
            }
        });

        read.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                BluetoothGattCharacteristic characteristic = mGattCharacteristics.get(3).get(0);

                mBluetoothLeService.readCharacteristic(characteristic);
                Log.d("TEST", "Read");

            }
        });

        disconnect.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mBluetoothLeService.disconnect();
                Log.d("TEST", "Disconnect");
            }
        });

        getActionBar().setTitle(mDeviceName);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        mBluetoothLeScanner = BluetoothAdapter.getDefaultAdapter().getBluetoothLeScanner();
        mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        startBTScan();
    }

    private void startBTScan()
    {
//        if (mBluetoothLeService != null) {
//            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
//            Log.d(TAG, "Connect request result=" + result);
//        }

        List<ScanFilter> filters = new ArrayList<ScanFilter>();

//        ScanFilter filter = new ScanFilter.Builder()
//                .setServiceUuid( new ParcelUuid(UUID.fromString(getString(R.string.ble_uuid)) ) )
//                .build();
//        filters.add( filter );

        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode( ScanSettings.SCAN_MODE_LOW_LATENCY )
                .setReportDelay(0)
                .build();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && scantypenew)
            {
                if(mBluetoothLeScanner != null)
                {
                    mBluetoothLeScanner.startScan(filters, settings, mScanCallback);
                }
            }
            else
            {
                mBluetoothAdapter = mBluetoothManager.getAdapter();
                if (mBluetoothAdapter != null) {
                    mBluetoothAdapter.startLeScan(new BluetoothAdapter.LeScanCallback(){
                        @Override
                        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                            // get the discovered device as you wish
                            if(device.getAddress().equalsIgnoreCase(mDeviceAddress))
                            {
                                String name = device.getName();
                                setDataReceived(name);
                                stopOldScan();
                            }
                        }
                    });
                }
            }

        startwait = false;
    }

    private void stopOldScan()
    {
        mBluetoothAdapter.stopLeScan(new BluetoothAdapter.LeScanCallback(){
            @Override
            public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                if(! scantypenew)
                {
                    startwait = true;
                    oldstart.postDelayed(r1, START_DELAY);
                }
            }
        });
    }

    private Handler oldstart = new Handler();

    Runnable r1 = new Runnable() {

        @Override
        public void run() {
            startBTScan();
        }
    };

    @Override
    protected void onPause()
    {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);

        if(mBluetoothLeScanner != null)
            mBluetoothLeScanner.stopScan(mScanCallback);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_device, menu);
//        if (mConnected) {
//            menu.findItem(R.id.menu_connect).setVisible(false);
//            menu.findItem(R.id.menu_disconnect).setVisible(true);
//        } else {
//            menu.findItem(R.id.menu_connect).setVisible(true);
//            menu.findItem(R.id.menu_disconnect).setVisible(false);
//        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()) {
//            case R.id.menu_connect:
//                mBluetoothLeService.connect(mDeviceAddress);
//                return true;
//            case R.id.menu_disconnect:
//                mBluetoothLeService.disconnect();
//                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectionState.setText(resourceId);
            }
        });
    }

    private void displayData(String data) {
        if (data != null) {
            mDataRead.setText(data);
        }
    }

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
//            currentServiceData.put(
//                    LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
//                currentCharaData.put(
//                        LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }

        SimpleExpandableListAdapter gattServiceAdapter = new SimpleExpandableListAdapter(
                this,
                gattServiceData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_UUID},
                new int[] { android.R.id.text1, android.R.id.text2 },
                gattCharacteristicData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_UUID},
                new int[] { android.R.id.text1, android.R.id.text2 }
        );
        mGattServicesList.setAdapter(gattServiceAdapter);
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_UNAVAILABLE);
        intentFilter.addAction(BluetoothLeService.OPERATION_EXECUTED);
        intentFilter.addAction(BluetoothLeService.OPERATION_FAILED);
        intentFilter.addAction(BluetoothLeService.RELIABLE_EXECUTED);
        intentFilter.addAction(BluetoothLeService.RELIABLE_FAILED);
        return intentFilter;
    }

    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            if( result == null
                    || result.getDevice() == null
                    || TextUtils.isEmpty(result.getDevice().getName()) )
                return;

//            StringBuilder builder = new StringBuilder( result.getDevice().getName() );
            StringBuilder builder = new StringBuilder( result.getScanRecord().getDeviceName() );
            String addr = result.getDevice().getAddress();

            if(! addr.equalsIgnoreCase(mDeviceAddress))
                return;

            setDataReceived(result.getScanRecord().getDeviceName());

//            builder.append("\n").append(new String(result.getScanRecord().getServiceData(result.getScanRecord().getServiceUuids().get(0)), Charset.forName("UTF-8")));
            Map<ParcelUuid, byte[]> data = result.getScanRecord().getServiceData();

//            if(data == null || data.isEmpty())
//                builder.append(" - " + getResources().getString(R.string.no_data));
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.e("BLE", "Discovery onScanFailed: " + errorCode);
            super.onScanFailed(errorCode);
        }
    };

    private void setDataReceived(String name)
    {
        StringBuilder builder = new StringBuilder( name );
        String toAnalyze = name.substring(7, 9);

        int value = Integer.decode("0x" + toAnalyze);

        int control = value & 0x08;

        lockState = Integer.decode("0x" + toAnalyze) & 0x20;

        if(control == 0)
            lamp.setBackgroundColor(Color.parseColor("#00ff00"));
        else
            lamp.setBackgroundColor(Color.parseColor("#ff0000"));


        if(lockState == 0)
            lock.setBackgroundColor(Color.parseColor("#00ff00"));
        else if(lockState > 0)
            lock.setBackgroundColor(Color.parseColor("#ff0000"));
        else
            lock.setBackgroundColor(Color.parseColor("#000000"));

        builder.append(" - " + control);

        builder.append(" - " + lockState);

        mDataField.setText(builder.toString());
    }
}
